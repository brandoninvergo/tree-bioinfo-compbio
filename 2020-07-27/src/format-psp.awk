{
    # Take only human kinases & substrates
    # Only use STY phosphorylation
    if (($4 == "human" &&
         $9 == "human" &&
         $12 ~ /[A-Za-z_]{7}[sty][A-Za-z_]{7}/)){
        if ($8 != "")
            print $1, $8, $10, $12
        else
            print $1, $7, $10, $12
    }
}
