library(mclust)
library(corrplot)
library(viridis)
library(tidyverse)
library(cowplot)
library(ggseqlogo)
## From Bioconductor:
library(ReactomePA)
library(clusterProfiler)
library(org.Hs.eg.db)

theme_set(theme_cowplot())

## Read in the data.  The file doesn't have any column names so we
## have to add them manually.  In the data-formatting script, we
## omitted the 0s timepoint, so we're left with 5s, 10s, 30s and 60s
## for both EGF and IGF stimuli.
tc_data <- readr::read_tsv("data/wilkes-timecourse.tsv",
                           col_names = c("protein",
                                         "site",
                                         "seq.win",
                                         paste0("EGF", c(5, 10, 30, 60)),
                                         paste0("IGF", c(5, 10, 30, 60))))

## We only need the quantitative data for clustering and it should be
## in a matrix format, not a data.frame (or tibble)
tc_data_m <- dplyr::select(tc_data, EGF5:IGF60) %>%
    as.matrix()

## There's a random element to the clustering method so set the random
## seed so that we always get the same results.
set.seed(1)

## Mclust is a clustering method based on Gaussian mixture models.  We
## will cluster simultaneously on the EGF and IGF timecourses, so
## sites in the same cluster will have similar responses across the
## two stimuli.  Mclust finds the best-fitting model for a range of
## numbers of clusters and for different constraints on the model.
## Here I'm testing up to 30(!) clusters.
tc_clusts <- mclust::Mclust(tc_data_m, G = 1:30)

## You can do various diagnostic plots of the fit
plot(tc_clusts, "BIC")

## Look at how the different timepoints relate to each other with
## regards to site-cluster classification
plot(tc_clusts, "classification")

## Add the cluster assignments to the original table and format it to
## be ggplot2-friendly.  So, we want only one "value" column with the
## log2 fold changes, we want separate "stimulus" and "timepoint"
## columns which will be extracted from the column names ("EGF5" ->
## "EGF" and "5") and then we want to add a column that can group the
## values of a single site.
tc_data_long <- dplyr::mutate(tc_data, cluster = tc_clusts$classification) %>%
    tidyr::pivot_longer(cols = EGF5:IGF60, names_to = "sample", values_to = "log2fc") %>%
    tidyr::extract(sample, c("stimulus", "timepoint"), "([EI]GF)([01356]0?)",
            convert = TRUE) %>%
    tidyr::unite(trace, protein, site, stimulus, sep="-", remove = FALSE) %>%
    group_by(trace)

## Make the cluster labels more readable for a figure
tc_data_long$cluster <- factor(tc_data_long$cluster,
                               levels = sort(unique(tc_data_long$cluster)),
                               labels = paste("cluster", sort(unique(tc_data_long$cluster))))

## Make an auxiliary table with mean log2 fold changes for each
## cluster.  We can add these to the plot for easier visualisation.
tc_data_sum <- dplyr::group_by(tc_data_long, cluster, stimulus, timepoint) %>%
    dplyr::summarise(mean.log2fc = mean(log2fc))

## Produce a plot with one panel per cluster, showing a trace for each
## timecourse for each stimulus.  Also include a loess-smoothed mean
## trace.
tc_clust_plot <- ggplot2::ggplot(tc_data_long, ggplot2::aes(timepoint, log2fc, color = stimulus, group = stimulus)) +
    ggplot2::geom_hline(color = "gray", yintercept = 0, linetype = 2, size = 0.75) +
    ggplot2::geom_line(ggplot2::aes(group = trace), size = 1, alpha = 0.05) +
    ggplot2::geom_smooth(method = "loess", size = 1.5, se = FALSE) +
    ggplot2::scale_x_continuous(breaks = c(5, 10, 30, 60)) +
    ggplot2::ylab(expression(paste(log[2], "(fold change)"))) +
    ggplot2::xlab("time (s)") +
    ggplot2::facet_wrap(~cluster, scales = "free_y")
cowplot::save_plot("img/tc-clusters.png", tc_clust_plot, cols = 6, rows = 7,
                   base_height = 1.5, base_asp = 2)

## We can do a pathway enrichment analysis on the clusters using the
## ReactomePA and clusterProfiler packages from Bioconductor.
## We need the IDs in Entrez form, so first get those using clusterProfiler::bitr:
prot_id_map <- clusterProfiler::bitr(unique(tc_data$protein), "SYMBOL",
                                     "ENTREZID", "org.Hs.eg.db")
rownames(prot_id_map) <- prot_id_map$SYMBOL
tc_clust_l <- list()
## Reformat the timecourse cluster classifications as a list
num_clusts <- max(tc_clusts$classification)
for (i in 1:num_clusts){
    cluster_psites <- which(tc_clusts$classification == i)
    cluster_prots <- unique(tc_data$protein[cluster_psites])
    cluster_prots_entrez <- prot_id_map[cluster_prots, "ENTREZID"]
    tc_clust_l[[paste("cluster", i)]] <- cluster_prots_entrez
}
## Do the enrichment analysis
tc_enrich <- clusterProfiler::compareCluster(tc_clust_l, fun = "enrichPathway")
## Plot it
tc_clust_plot <- clusterProfiler::dotplot(tc_enrich) +
    theme(axis.text.x = element_text(angle = 45, hjust = 1, vjust = 1))

cowplot::save_plot("img/tc-cluster-pathways.png", tc_clust_plot, base_height = 12)

## Just one sequence logo, as an example
clust1_psites <- which(tc_clusts$classification == 1)
clust1_seqwins <- tc_data$seq.win[clust1_psites]
clust1_seqlogo <- ggseqlogo::ggseqlogo(clust1_seqwins) +
    ggplot2::ggtitle("cluster 1 sequence logo")
cowplot::save_plot("img/cluster-1-logo.png", clust1_seqlogo)

############################
## Kinase enrichment analysis

## Now that we have our clusters, we don't really need the
## quantitative data for the next steps.
tc_clust_small <- dplyr::select(tc_data, protein, site, seq.win) %>%
    dplyr::mutate(cluster = factor(tc_clusts$classification))

## Let's also pull in some kinase-substrate annotations from PhosphositePlus
psp <- readr::read_tsv("data/kinase-substrates.tsv",
                       col_names = c("kinase", "substrate", "site", "seq.win"))
## Merge the kinase-substrate annotations with the clustered sites.
## There are sites in our data that do not have annotated kinases and
## there are sites with annotated kinases that are not in our data.
## We're doing an "inner join" here, so we just take the intersection
## of these two sets: sites that have annotated kinases and that are
## in our data.  Note that some sites have multiple kinases annotated
## to them.
tc_clust_psp <- dplyr::inner_join(tc_clust_small, psp,
                                  by = c("protein" = "substrate",
                                         "site" = "site",
                                         "seq.win" = "seq.win"))
## Restrict just to kinases that have sufficent substrates in the
## dataset (arbitrary minimum, just to simplify the following
## situation a bit)
good_kins <- dplyr::group_by(tc_clust_psp, kinase) %>%
    dplyr::summarise(n = n()) %>%
    dplyr::filter(n >= 3) %>%
    dplyr::select(kinase) %>%
    tibble::deframe()
tc_clust_psp <- dplyr::filter(tc_clust_psp, kinase %in% good_kins)

## Get the list of all kinases that we know about:
kinases <- unique(tc_clust_psp$kinase)

## Let's try a kinase enrichment.  This is a computationally slow way
## of doing it but it should be a bit clearer for didactic purposes.
kinase_col <- c()
cluster_col <- c()
pval_col <- c()
## Total number of kinase-substrate annotations (our "background")
n <- nrow(tc_clust_psp)
for (kin in kinases){
    ## Total number of substrates assigned to this kinase
    num_sub <- dplyr::filter(tc_clust_psp, kinase == kin) %>% nrow()
    for (clust in 1:max(as.numeric(tc_clust_psp$cluster))){
        ## Total number of sites in this cluster
        num_clust <- dplyr::filter(tc_clust_psp,
                                   cluster == clust) %>% nrow()
        ## Total number of this kinase's substrates in this cluster
        num_clust_sub <- dplyr::filter(tc_clust_psp,
                                       kinase == kin,
                                       cluster == clust) %>% nrow()
        if (num_clust > 0){
            ## Do a binomial test for enrichment
            p <- binom.test(num_clust_sub, num_clust, p = num_sub/n,
                            alternative = "greater")$p.value
            ## Build up some vectors of results that will be used to make
            ## a table
            kinase_col <- c(kinase_col, kin)
            cluster_col <- c(cluster_col, clust)
            pval_col <- c(pval_col, p)
        }else{
            ## The kinase doesn't have any substrates in this cluster
            kinase_col <- c(kinase_col, kin)
            cluster_col <- c(cluster_col, clust)
            pval_col <- c(pval_col, NA)
        }
    }
}

## Build a table of the results in tibble format
kinase_enrich <- tibble::tibble(kinase = kinase_col,
                        cluster = cluster_col,
                        p = pval_col,
                        logp = -log10(pval_col)) %>%
    dplyr::mutate(p.adjust = p.adjust(p, method = "fdr"))
kinase_enrich$cluster <- factor(kinase_enrich$cluster)
## Are any of them significant?
dplyr::filter(kinase_enrich, p.adjust < 0.05)

## Reordering the rows (kinases) and columns (clusters) via
## hierarchical clustering could reveal some interesting patterns
## First, kinase order:
enrich_data_w <- dplyr::select(kinase_enrich, -p, -p.adjust) %>%
    tidyr::pivot_wider(names_from = "cluster", values_from = "logp")
enrich_data_w[is.na(enrich_data_w)] <- 0
enrich_data <- dplyr::select(enrich_data_w, -kinase) %>%
    as.matrix() %>%
    scale()
kin_order <- hclust(dist(enrich_data, method = "euclidean"), method = "ward.D")$order
kinase_enrich$kinase <- factor(kinase_enrich$kinase,
                               levels = unique(kinase_enrich$kinase)[kin_order])
## Now cluster order, same basic procedure:
enrich_data <- dplyr::select(enrich_data_w, -kinase) %>%
    as.matrix() %>%
    t() %>%
    scale()
clust_order <- hclust(dist(enrich_data, method = "euclidean"), method = "ward.D")$order
kinase_enrich$cluster <- factor(kinase_enrich$cluster,
                                levels = unique(kinase_enrich$cluster)[clust_order])

## Do the plot as a heatmap showing the -log10 p-value of the kinase-cluster enrichment
kin_enrich_plot <- ggplot2::ggplot(kinase_enrich, ggplot2::aes(x = cluster, y = kinase, fill = logp)) +
    ggplot2::geom_tile() +
    ggplot2::scale_fill_viridis()
cowplot::save_plot("img/cluster-kinase-enrich.png", kin_enrich_plot,
                   base_height = 20, base_asp = 1/1.618)

## Is there any structure to these results?
## Reformat to a matrix structure
kinase_enrich_w <- dplyr::select(kinase_enrich, kinase, cluster, p) %>%
    tidyr::pivot_wider(names_from = "cluster", values_from = "p")
kinase_enrich_m <- dplyr::select(kinase_enrich_w, -kinase) %>%
    as.matrix()
rownames(kinase_enrich_m) <- kinase_enrich_w$kinase

## Calculate correlation between kinases, then look at how well they
## cluster together.
kinase_enrich_cor <- cor(t(kinase_enrich_m), use = "pairwise.complete.obs")
kinase_clust <- Mclust(kinase_enrich_cor)$classification
corrplot(kinase_enrich_cor[order(kinase_clust),
                           order(kinase_clust)], addrect = max(kinase_clust))

## So, not great...not really clear.  But here's a trick.  We can try
## the correlation of correlations to try to better resolve similar
## kinases.  That is, which kinases correlate with the other kinases
## similarly to kinase X?
kinase_enrich_cor <- cor(cor(t(kinase_enrich_m), use = "pairwise.complete.obs"))
kinase_clust <- Mclust(kinase_enrich_cor)$classification
corrplot(kinase_enrich_cor[order(kinase_clust),
                           order(kinase_clust)])
## That's better.  Just glancing at the correlation plot shows some
## reasonable associations.
png("img/kinase-enrich-correlation.png", width = 1000, height = 1000)
corrplot::corrplot(kinase_enrich_cor[order(kinase_clust),
                                     order(kinase_clust)])
dev.off()

## Now, do these clusters of kinases make biological sense?  That is,
## are they functionally related?  We can do a pathway enrichment
## analysis.  Here I'll use ReactomePA from Bioconductor.

## ReactomePA needs Entrez IDs, so use bitr from clusterProfiler to
## convert from HGNC symbols to Entrez (welcome to how a computational
## biologist spends 50% of their time: converting IDs)
id_map <- clusterProfiler::bitr(kinases, "SYMBOL", "ENTREZID", "org.Hs.eg.db")
rownames(id_map) <- id_map$SYMBOL
kinases_entrez <- id_map[kinases, "ENTREZID"]
names(kinase_clust) <- kinases_entrez

## We also need to reformat our kinase cluster assignments to a list
## format.
num_clusts <- max(kinase_clust)
kinase_clust_l <- list()
for (i in 1:num_clusts){
    cluster_kinases <- names(kinase_clust[kinase_clust == i])
    kinase_clust_l[[paste("cluster", i)]] <- cluster_kinases
}

## Use compareCluster from clusterProfiler, tell it to call
## enrichPathway from ReactomePA.
path_enrich <- clusterProfiler::compareCluster(kinase_clust_l, fun = "enrichPathway")

kinase_clust_plot <- clusterProfiler::dotplot(path_enrich) +
    ggplot2::theme(axis.text.x = element_text(angle = 45, hjust = 1, vjust = 1))
cowplot::save_plot("img/kinase-cluster-pathways.png", kinase_clust_plot,
                   base_height = 8)

