import sys
import re
from Bio import SeqIO


# A (very loose but functional in this limited case) regular
# expression for finding the Uniprot ID.  We want the whole ID.
PROT_ID_RE = re.compile(r"^([A-Za-z0-9-]+)")
# A regular expression for finding a phosphosite in the format "p-S52"
# (meaning "phosphorylated serine at residue 52").  We want to capture
# the residue and the position, dropping the "p-".
SITES_RE = re.compile(r"p-([STY][0-9]+)")
# A regular expression for identifying the HGNC gene name.  Often when
# presenting results, these are clearer than Uniprot IDs (noting, of
# course, that one HGNC gene ID can map to multiple Uniprot IDs).  In
# the Wilkes et al peptide db, these are part of a larger gene
# description in the format "GN=BRCA2".  We want to capture the gene
# name but drop the "GN="
HGNC_RE = re.compile(r"GN=([A-Za-z0-9-]+)")


def parse_psite_id(psite_id):
    """Parse the "Phosphopeptide ID (format 2)" column in the peptide
database.  This is in the format HGNCID p-S1 (z= N) or, if there are multiple
sites HGNCID p-S1 p-T2 (z= N)

    """
    prot_id = re.search(PROT_ID_RE, psite_id)
    try:
        assert prot_id
    except AssertionError:
        sys.exit("Error parsing protname in psite id " + psite_id)
    sites = re.findall(SITES_RE, psite_id)
    return (prot_id.groups()[0], sites)


def parse_peptide_db(pepdb_h):
    """Parse the peptide database associated with the quantitative data."""
    pepdb = dict()
    # skip header
    pepdb_h.readline()
    for line in pepdb_h:
        # Only get the columns that interest us: the internal database
        # ID, the phosphosite ID, and the site list
        line_spl = line.strip().split("\t")
        db_id = line_spl[30]
        psite_id = line_spl[27]
        sites = parse_psite_id(psite_id)
        pepdb[db_id] = sites
    return pepdb


def parse_quant_file(quant_h, skip):
    """Parse the data sheet of a phosphoproteomics file from Wilkes et al.
Different date sets from the publication follow slightly different
formatting.  This is generalised to all of them.

    """
    quant = dict()
    # skip first header line
    quant_h.readline()
    # The data files first contain columns with the log2 fold changes
    # and then they contain columns with p-values.  Both sets of
    # columns are preceded by a "Database ID" column.  So, pull apart
    # the main header line to figure out where the second "Database
    # ID" is in order to safely omit the p-values.
    header = quant_h.readline()
    header_spl = header.strip().split("\t")
    header_spl.reverse()
    try:
        cut = header_spl.index("Database ID") + 1
    except ValueError:
        cut = header_spl.index("db ID") + 1
    for line in quant_h:
        line_spl = line.strip().split("\t")
        db_id = line_spl[2]
        quant_data = line_spl[3:-cut]
        pvals = line_spl[-cut+1:]
        # Don't take rows with missing values
        if "NA" in pvals:
            continue
        is_sig = [float(p) < 0.05 for p in pvals]
        # Only take rows with at least one significant time point
        if True not in is_sig:
            continue
        quant_data = [float(val) for n, val in enumerate(quant_data)
                      if n not in skip]
        quant[db_id] = quant_data
    return quant


def parse_proteome(proteome_h):
    """Parse the proteome in FASTA format."""
    proteome_it = SeqIO.parse(proteome_h, "fasta")
    proteome = dict()
    for seqrec in proteome_it:
        hgnc_match = re.search(HGNC_RE, seqrec.description)
        try:
            assert hgnc_match
        except AssertionError:
            sys.exit("Error parsing proteome: " + seqrec)
        hgnc_id = hgnc_match.groups()[0]
        proteome[hgnc_id] = str(seqrec.seq)
    return proteome


def seq_window(prot_id, seq, site_str, winlen):
    """Construct a fixed-length sequence window around a phosphosite.
Windows near a protein terminus should be padded with _ characters.
Phosphosite positions are fed in indexed from 1.

    """
    res = site_str[0]
    site = int(site_str[1:])
    # Convert to 0-indexing
    site -= 1
    half = winlen//2
    try:
        assert(site >= 0 and site < len(seq))
    except AssertionError:
        return ("NA", "NA")
    try:
        assert(seq[site] in ('S', 'T', 'Y'))
    except AssertionError:
        sys.stderr.write("Bad seq: " + prot_id + " " + site_str + "\n")
        return ("NA", "NA")
    try:
        assert(seq[site] == res)
    except AssertionError:
        sys.stderr.write("Wrong residue: " + prot_id + " - " + site_str + "\n")
        return ("NA", "NA")
    start = site-half
    end = site+half+1
    if start < 0:
        startstr = -start * "_" + seq[0:(half+start)+1]
    else:
        startstr = seq[start:start+half+1]
    if end > len(seq):
        endstr = seq[start+half+1:len(seq)] + (end-len(seq)) * "_"
    else:
        endstr = seq[start+half+1:end]
    return("-".join([prot_id, str(site+1)]), startstr+endstr)


def format_data(pepdb, quant, proteome):
    """Combine the quantitative data table with the peptide database and
the proteome to get the format that we want.

    """
    data = dict()
    counts = dict()
    # Each row in the quantitative data is associated with a database ID
    for db_id, quant_data in quant.items():
        # Get the associated protein ID and sites from the peptide database
        prot_id, sites = pepdb[db_id]
        # Try to get the full protein sequence from the proteome
        try:
            prot_seq = proteome[prot_id]
        except KeyError:
            continue
        # quant_range = calc_quant_range(quant_data)
        # if quant_range == "NA":
        #     continue
        for site in sites:
            site_id = prot_id + "_" + site
            # If a site is observed on multiple peptides, 
            if site_id not in data:
                # Construct a sequence window for the phosphosite
                seqwin = seq_window(prot_id, prot_seq, site, 15)[1]
                if seqwin == "NA":
                    continue
                # Add the quantitative data
                data[site_id] = [seqwin, quant_data]
                # Initiate a count
                counts[site_id] = 1
            else:
                # Add the quantitative data to the existing sum and
                # increase the count
                data[site_id][1] = [data[site_id][1][i] + quant_data[i] for
                                    i in range(len(quant_data))]
                counts[site_id] += 1
        for site in sites:
            if site not in counts:
                continue
            count = counts[site_id]
            if count == 1:
                continue
            # More than one occurrence of the site, so calculate the
            # mean fold-change
            quant_data = data[site_id][1]
            data[site_id][1] = [quant_data[i] / count for
                                i in range(len(quant_data))]
    return data


def print_data(data):
    for site_id, site_data in data.items():
        seqwin, quant_data = site_data
        prot_id, site = site_id.split("_")
        line = [prot_id, site, seqwin]
        line.extend([str(x) for x in quant_data])
        print("\t".join(line))


if __name__ == "__main__":
    if len(sys.argv) != 4:
        sys.exit("USAGE: <script> PEPTIDE_DB QUANT_FILE PROTEOME")
    pepdb_file = sys.argv[1]
    quant_file = sys.argv[2]
    proteome_file = sys.argv[3]
    with open(pepdb_file) as h:
        pepdb = parse_peptide_db(h)
    # Don't include parental/baseline conditions from the quantitative
    # data because they're always equal to zero
    if "timecourse" in quant_file:
        skip = [0, 5]
    elif "stoich" in quant_file:
        skip = [0]
    else:
        skip = []
    with open(quant_file) as h:
        quant = parse_quant_file(h, skip)
    with open(proteome_file) as h:
        proteome = parse_proteome(h)
    data = format_data(pepdb, quant, proteome)
    print_data(data)
