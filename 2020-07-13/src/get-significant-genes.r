## In this script, we'll load up the full differential expression
## results file (produced by the DESeq2 package for R).  Then we'll
## filter it to keep only transcripts that are significantly
## differentially expressed, then we'll get the associated genes, and
## then we'll write the significant genes to a new file.

## Sometimes it suffices to do everything within one R script, so we
## wouldn't have to write the results to a new file.  However, since
## we will be using some external tools, in this case it's a
## requirement.

## Also, although I won't be delving too much into programming, I'll
## demonstrate a few different ways to accomplish this within R.
## Which one you use is a matter of preference.

## I'm going to use the popular "Tidyverse" package for R.  This
## drastically changes the coding style, but I find it easier to
## understand at a glance.
library(tidyverse)

## Read in the data.  Tidyverse doesn't like rownames, so the first
## column will automatically be given the name "X1".  The rownames are
## just the Ensembl transcript ID, but we already have a column in the
## dataset with transcript IDs (column 6 in the DESeq2 output).  So,
## we can just remove the X1 column.

## Tidyverse allows long chains of commands using the %>% pipe
## operator, which I will use, but I think it's best still to break
## things into explicit steps rather than making one huge chain.  This
## helps with the readability / clarity of your code, which will help
## when someone else (including Future You) has to read it.
expression <- read_csv("data/topqlf.cpm.7.csv") %>%
    select(-X1)
## Get all the transcripts that have a p-value < 0.05 after adjusting
## for False Discovery Rate (FDR; Benjamini & Hochberg 1995) and that
## encode genes (see below); i.e. the gene ID is not missing
sig_expression <- filter(expression, FDR < 0.05, !is.na(zfin_id_id))
## Write the table in case we want to use it in the future (creating a
## new output directory if it doesn't exist).
if (!dir.exists("out"))
    dir.create("out")
write_tsv(sig_expression, "out/sig-transcripts.tsv")

## This table is indexed by transcripts but of course you can have
## multiple transcripts per gene.

## Gene Ontology terms are, unsurprisingly, associated with genes.
## So, in the end, we need a list of genes that have undergone
## significant differential expression (for at least one isoform).
## Note: for zebrafish, the GO association file uses the zfin ID, not
## Ensembl ID, so that's what I'll use here.

## Just out of curiosity, how many transcripts per gene do we see
mean(table(sig_expression$zfin_id_id))
max(table(sig_expression$zfin_id_id))
## So there are on average 1.25 transcripts per gene, up to 9
## transcripts max.

## Since we already have our significant transcripts, the simplest way
## to do this is to get all the unique gene IDs associated with them.
sig_genes <- unique(sig_expression$zfin_id_id)

## Write this list to a text file.  It's not a table, so we can't use
## write_tsv without jumping through some hoops.
write.table(sig_genes, "out/sig-genes.txt", quote = FALSE, col.names = FALSE,
            row.names = FALSE)

## Maybe we're interested in knowing whether some processes are down
## regulated while others are up-regulated.  Let's take all the
## down-regulated genes and all the up-regulated genes.  Transcript
## fold changes are stored in the logFC column.  As the name
## indicates, these are log2 transformed, so down-regulated genes have
## negative values.

## However, it could be that one gene has some transcripts
## up-regulated and others down-regulated.  We can check this by
## grouping the transcripts by gene and then looking for genes with a
## minimum logFC < 0 and a maximum logFC > 0
group_by(sig_expression, zfin_id_id) %>%
    summarise(minFC = min(logFC),
              maxFC = max(logFC),
              meanFC = mean(logFC),
              num.transcripts = n()) %>%
    filter(minFC < 0,
           maxFC > 0)
## 901 genes (out of 10457 significant genes, or about 9%) have some
## transcripts up-regulated and others down-regulated.

## There are two ways you could handle this, and which one you choose
## depends on the biology.  On the one hand, you can simply filter by
## logFC (e.g. for down-reg, logFC < 0) and then again take all the
## unique gene IDs.  This way a single gene ID can end up in both
## down- and up-regulated gene lists.  On the other hand, you can use
## the mean or median expression for each gene to decide whether it's
## generally up or down regulated.  For the case at hand, I think the
## latter is the better choice: in the above results, we see some
## genes effectively switching transcript, but the mean FC is around
## 0.  So, the mean FC captures the overall change in the gene's
## expression level.
mean_gene_expression <- group_by(sig_expression, zfin_id_id) %>%
    summarise(meanFC = mean(logFC))
down_expression <- filter(mean_gene_expression, meanFC < 0)
up_expression <- filter(mean_gene_expression, meanFC > 0)

## Write those gene lists to text files
write.table(unique(down_expression$zfin_id_id), "out/sig-down-genes.txt",
            quote = FALSE, col.names = FALSE, row.names = FALSE)
write.table(unique(up_expression$zfin_id_id), "out/sig-up-genes.txt",
            quote = FALSE, col.names = FALSE, row.names = FALSE)

## Finally, for GO enrichment analysis, we need a "background" set of
## genes.  What you choose as your background depends on your
## experiment and what you want to ask.  For differential expression
## analysis, it makes sense to just use the list of all expressed
## genes.  You could also use the entire genome if you want to, but
## here I think you want to know "given the genes that are expressed
## in my samples, what is special about the ones that change
## expression in my experimental condition?."  Using the data that I
## have on hand, I'm going to assume that any transcript that is
## expressed in the samples will have a differential expression FC
## calculated.  So, simply take all unique gene IDs from the full data set.
all_genes <- unique(expression$zfin_id_id)
write.table(all_genes, "out/all-genes.txt", quote = FALSE, col.names = FALSE,
            row.names = FALSE)
