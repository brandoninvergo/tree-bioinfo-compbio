library(tidyverse)
library(venn)

## Load up all the Biological Process term enrichments for each
## method.  Each method's output is different so we need to filter appropriately.

## GOATOOLS: need FDR < 0.05
goa_all_enrich_terms <- read_tsv("out/enrichment/goa-sig-genes-enrichment-bp.tsv") %>%
    filter(p_fdr_bh < 0.05) %>%
    select(`# GO`) %>%
    deframe()
goa_up_enrich_terms <- read_tsv("out/enrichment/goa-sig-up-genes-enrichment-bp.tsv") %>%
    filter(p_fdr_bh < 0.05) %>%
    select(`# GO`) %>%
    deframe()
goa_down_enrich_terms <- read_tsv("out/enrichment/goa-sig-down-genes-enrichment-bp.tsv") %>%
    filter(p_fdr_bh < 0.05) %>%
    select(`# GO`) %>%
    deframe()

## PantherDB: everything's significant, but GO IDs are mixed with term
## descriptions, so the IDs need to be extracted
pantherdb_all_enrich_terms <- read_tsv("out/enrichment/pantherdb-sig-genes-enrichment-bp.tsv") %>%
    mutate(term = sub(".*\\((GO:[[:digit:]]{7})\\)", "\\1",
                      `GO biological process complete`,
                      perl = TRUE)) %>%
    select(term) %>%
    deframe()
pantherdb_up_enrich_terms <- read_tsv("out/enrichment/pantherdb-sig-up-genes-enrichment-bp.tsv") %>%
    mutate(term = sub(".*\\((GO:[[:digit:]]{7})\\)", "\\1",
                      `GO biological process complete`,
                      perl = TRUE)) %>%
    select(term) %>%
    deframe()
pantherdb_down_enrich_terms <- read_tsv("out/enrichment/pantherdb-sig-down-genes-enrichment-bp.tsv") %>%
    mutate(term = sub(".*\\((GO:[[:digit:]]{7})\\)", "\\1",
                      `GO biological process complete`,
                      perl = TRUE)) %>%
    select(term) %>%
    deframe()

## MGSA: take any term that showed up with marginal probability >0.5
## in more than 1 run
mgsa_all_enrich_terms <- read_tsv("out/enrichment/mgsa-sig-genes-enrichment-bp.tsv",
                              col_names = c("count", "term", "description")) %>%
    filter(count > 1) %>%
    select(term) %>%
    deframe()
mgsa_up_enrich_terms <- read_tsv("out/enrichment/mgsa-sig-up-genes-enrichment-bp.tsv",
                              col_names = c("count", "term", "description")) %>%
    filter(count > 1) %>%
    select(term) %>%
    deframe()
mgsa_down_enrich_terms <- read_tsv("out/enrichment/mgsa-sig-down-genes-enrichment-bp.tsv",
                              col_names = c("count", "term", "description")) %>%
    filter(count > 1) %>%
    select(term) %>%
    deframe()

## Also load the terms enriched in the provided Gprofiler2 results
## file, taking (already corrected) p-values < 0.05.  The test was
## done on all 3 GO hierarchies, so it's not a 100% apples-to-apples
## comparison, but we'll just go ahead and take the Biological Process
## terms.
gprof_all_enrich_terms <- read_tsv("data/gem_7.csv") %>%
    filter(p_value < 0.05, source == "GO:BP") %>%
    select(term_id) %>%
    deframe()

## Make a Venn diagram for term enrichment when using the list of all
## significantly differentially expressed genes
all_enrich_terms <- list("GOATOOLS" = goa_all_enrich_terms,
                         "PantherDB" = pantherdb_all_enrich_terms,
                         "MGSA" = mgsa_all_enrich_terms,
                         "Gprofiler2" = gprof_all_enrich_terms)
png("img/sig-genes-enrich-overlap.png")
venn(all_enrich_terms, ggplot = FALSE, ilcs = 1.5, sncs = 1.75, box = FALSE)
dev.off()

## And now the same for up- and down-regulated genes
up_enrich_terms <- list("GOATOOLS" = goa_up_enrich_terms,
                        "PantherDB" = pantherdb_up_enrich_terms,
                        "MGSA" = mgsa_up_enrich_terms)
png("img/sig-up-genes-enrich-overlap.png")
venn(up_enrich_terms, ggplot = FALSE, ilcs = 1.5, sncs = 1.75, box = FALSE)
dev.off()

down_enrich_terms <- list("GOATOOLS" = goa_down_enrich_terms,
                          "PantherDB" = pantherdb_down_enrich_terms,
                          "MGSA" = mgsa_down_enrich_terms)
png("img/sig-down-genes-enrich-overlap.png")
venn(down_enrich_terms, ggplot = FALSE, ilcs = 1.5, sncs = 1.75, box = FALSE)
dev.off()
