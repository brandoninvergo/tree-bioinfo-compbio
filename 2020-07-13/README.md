# 2020 July 13: Gene Ontology Enrichment Analysis

In this session, we analysed some gene differential expression data
(as produced by DESeq2) to determine whether certain biological
processes were over-represented among differentially expressed genes.

The analysis consists of three parts: getting the gene lists from the
transcript-based DESeq2 output (in the R script
`src/get-significant-genes.r`), performing various GO enrichment
analyses using command-line tools (in `Makefile`), and visualising the
results (in `Makefile`).  `Makefile` ties everything together into a
pipeline.

`src/enrichment-overlap.r` contains the code I used to produce the
Venn diagrams illustrating the overlap in results between the various
methods.

Note that the code, as written, requires access to the original data,
which is private.  Please use this code as guidelines on how to shape
your own analyses, rather than as a template.

## Required software

To run these analyses, I used:

* R + Tidyverse
* GOATOOLS: https://github.com/tanghaibao/goatools/
* Ontologizer: http://ontologizer.de

I ran everything in a Unix-like environment (GNU/Linux), so `Makefile`
contains some references to standard Unix tools.  Performing analyses
on Windows especially and possibly Mac OS may require alternative
tools (but this course is about the methods, not the software!).

## Structure of these files

`Makefile` is a file written for [GNU
Make](https://www.gnu.org/software/make).  Originally implemented for
defining how to build complex software, it is also very useful for
defining computational data analysis pipelines (where one analysis
might depend on the output of another analysis).  A full description
of the language is out of scope, but I have organised the file in a
hopefully easy-to-understand way:

1. Define the locations of files, so I can later refer to them by
   variable name (by "nickname" instead of having to write the full
   name).  As an aside, this is also convenient so if I ever decide to
   change a file name, I only need to change it in one place.
2. Phony targets: these are some more conveniences that can be ignored
   right now.  For example the phony target `prepare` means that I can
   just type `make prepare` on the command-line and it will download
   the GAF and OBO files for me.
3. Rules / recipes: These define how to create all of the different
   output files.  This is where the analyses are defined.  Rules
   follow the format of `outputfile: inputfile1 inputfile2` followed
   by Unix shell scripting.  I write the rules in a general way, so,
   e.g., there's one rule to create the separate "bioligical process"
   enrichment results for the list of all DE genes, the up-regulated
   genes, and the down-regulated genes.

Make is just one option.  Others include Snakemake and Common Workflow
Language.  If there is interest, I can do a special session on
defining pipelines for data analysis.
